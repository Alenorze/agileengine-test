# -*- coding: utf-8 -*-
# -----------------------------------------------------------------
# Name:         analyzer
# Purpose:      Simple web crawler that locates a user-selected element on a web site with frequently changing information
#
# Author:       Alexey Belov
#
# Created:      15/02/2020
# License:      GPL
# -----------------------------------------------------------------

import sys

from htmldom import htmldom



class Document(object):
    """
    Document object, presents html page
    """
    def __init__(self, html_file, selector=None):
        html = 'file:' + html_file
        self.dom = htmldom.HtmlDom(html).createDom()
        if selector == None:
            self.node = None
        else:
            self.node = self.dom.find(selector)

    def find(self, selector):
        node = self.dom.find(selector)
        return Node(node)

    def match(self, node):
        nodes = self.dom.find(node.node_name)
        best_match = {'node': None, 'count':0}
        for n in nodes:
            node = Node(n)
            matches = node.compare(node)
            if matches > best_match['count']:
                best_match['node'] = node
                best_match['count'] = matches

        return best_match['node']


class Node(object):
    """
    Node object, presents DOM node
    """
    def __init__(self, node):
        self.node = node

    def attr(self, name):
        if self.node:
            return self.attributes().get(name)
        else:
            return None

    def attributes(self):
        if self.node:
            if self.check_node_list():
                return self.node.nodeList[0].attributes
            else:
                return self.node.attributes

        return {}

    def check_node_list(self):
        return type(self.node) == htmldom.HtmlNodeList

    def get_node_parent(self, node):
        if type(node) == htmldom.HtmlNodeList:
            return node.nodeList[0].parentNode
        else:
            return node.parentNode

    def get_node_name(self, node):
        if type(node) == htmldom.HtmlNodeList:
            return node.nodeList[0].getName()
        else:
            return node.getName()

    @property
    def node_name(self):
        return self.get_node_name(self.node)

    def path(self):
        path = []
        if self.node:
            path.append(self.get_node_name(self.node))
            parent = self.get_node_parent(self.node)
            while parent:
                path.append(self.get_node_name(parent))
                parent = self.get_node_parent(parent)
            
        path.reverse()
        return ' > '.join(path)

    def compare(self, node):
        matches = 0
        if self.node:
            for attr in node.attributes():
                value = node.attr(attr)
                if attr in self.attributes() and value == self.attr(attr):
                    matches += 1

        return matches

    def text(self):
        if self.node:
            if self.check_node_list:
                return self.node.nodeList[0].getText()
            else:
                return self.node.getText()
        else:
            return None

    def html(self):
        if self.node:
            return self.node.html()
        else:
            return ''


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print('You did not specify the required arguments (python analyzer.py ref.html check.html id)')
        exit(1)

    html = Document(sys.argv[1])
    html2 = Document(sys.argv[2])
    tag_id = sys.argv[3]
    node = html.find("#{}".format(tag_id))

    if node == None:
        print('Tag ID not found')
        exit(1)

    found = html2.match(node)

    if found == None:
        print('No tag was be found')
        exit(1)

    print('Found')
    print(found.html())

    print('Path')
    print(found.path())
